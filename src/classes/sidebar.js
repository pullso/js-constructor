import { block } from '../utils'
import { ColumnsBlock, ImageBlock, TextBlock, TitleBlock } from './blocks'

export class Sidebar {
  constructor (selector, updateCallback) {
    this.$el = document.querySelector(selector)
    this.init()
    this.update = updateCallback
  }
  
  get template () {
    return [
      block('text'),
      block('title'),
      block('columns'),
      block('image'),
    ].join('')
  }
  
  init () {
    this.$el.insertAdjacentHTML('afterbegin', this.template)
    this.$el.addEventListener('submit', this.add.bind(this))
  }
  
  add (event) {
    event.preventDefault()
    
    const name = event.target.name
    const value = event.target.value.value
    const styles = event.target.styles.value
    
    let newBlock
    switch (name) {
      case 'text':
        newBlock = new TextBlock(value, {styles})
        break
      case 'title':
        newBlock = new TitleBlock(value, {styles})
        break
      case 'image':
        newBlock = new ImageBlock(value, {styles})
        break
      case 'columns':
        newBlock = new ColumnsBlock(value.split(';'), {styles})
        break
      default:
        break
    }
    this.update(newBlock)
    event.target.value.value = ''
    event.target.styles.value = ''
  }
}
