import image from './assets/image.png'
import { TitleBlock, ImageBlock, ColumnsBlock, TextBlock } from './classes/blocks'

const text = `
Сайт написан в качестве тренировки навыков программирования на Javascript :)!
`

export const model = [
  new TitleBlock('Конструктор сайтов на чистом JavaScript', {
    tag: 'h2',
    styles:
      {
        background: 'linear-gradient(to right, #FF00CC, #333399)',
        color: '#fff',
        'text-align': 'center',
        padding: '2rem',
      },
  }),
  new ImageBlock(image, {
    styles: {
      padding: '2rem 0',
      display: 'flex',
      'justify-content': 'center',
    },
    imageStyles: {
      width: '500px',
      height: 'auto',
    },
    alt: 'Это картинка',
  }),
  new ColumnsBlock( [
    'Проходя этот курс, я тренируюсь:',
    'Тренируюсь в написание кода на чистом JS',
    'Тренируюсь в ООП',
    'Тренируюсь в работе с новым сборщиком PARCEL',
  ], {
    styles: {
      background: 'linear-gradient(to bottom, #8e2de2, #4a00e0)',
      padding: '2rem',
      color: '#fff',
      'font-weight': 'bold',
    },
  }),
  new TextBlock( text, {
    styles: {
      background: 'linear-gradient(to left, #f2994a, #f2c94c)',
      padding: '2rem',
      color: '#fff',
      'font-weight': 'bold',
    },
  }),
]

//
// export const model = [
//   new TitleBlock('Конструктор сайтов на чистом JavaScript', {
//     tag: 'h2',
//     styles: {
//       background: 'linear-gradient(to right, #ff0099, #493240)',
//       color: 'green',
//       padding: '1.5rem',
//       'text-align': 'center'
//     }
//   }),
//   new ImageBlock(image, {
//     styles: {
//       padding: '2rem 0',
//       display: 'flex',
//       'justify-content': 'center'
//     },
//     imageStyles: {
//       width: '500px',
//       height: 'auto'
//     },
//     alt: 'Это картинка'
//   }),
//   new ColumnsBlock([
//     'Приложение на чистом JavaScript, без использования библиотек',
//     'Узнаешь как работают принципы SOLID и ООП в JavaScript за один курс',
//     'JavaScript - это просто, интересно. Научись создавать любые UI своими руками'
//   ], {
//     styles: {
//       background: 'linear-gradient(to bottom, #8e2de2, #4a00e0)',
//       padding: '2rem',
//       color: '#fff',
//       'font-weight': 'bold'
//     }
//   }),
//   new TextBlock(text, {
//     styles: {
//       background: 'linear-gradient(to left, #f2994a, #f2c94c)',
//       padding: '1rem',
//       'font-weight': 'bold'
//     }
//   })
// ]
